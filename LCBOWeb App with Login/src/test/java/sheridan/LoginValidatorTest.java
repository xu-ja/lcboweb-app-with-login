package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginNameRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName("ramses"));
	}
	
	@Test
	public void testIsValidLoginNameNegative() {
		assertFalse( "String is a palindrome", LoginValidator.isValidLoginName("#ramses@"));
	}
	
	@Test
	public void testIsValidLoginNameBoundaryIn() {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName("ramse1"));
	}
	
	@Test
	public void testIsValidLoginNameBoundaryout() {
		assertFalse( "String is a palindrome", LoginValidator.isValidLoginName("ramses_"));
	}
	
	@Test
	public void testIsValidLoginNameMinCharactersRegular() {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName("ramses123"));
	}
	
	@Test
	public void testIsValidLoginNameMinCharactersNegative() {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName("r1"));
	}
	
	@Test
	public void testIsValidLoginNameMinCharactersBoundaryIn() {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName("ramse1"));
	}
	
	@Test
	public void testIsValidLoginNameMinCharactersBoundaryOut() {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName("rams1"));
	}
}
