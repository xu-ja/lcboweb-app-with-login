package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		
		String loginNameRegex = "^[A-Za-z0-9]{6,}$";
		Pattern loginNamePattern = Pattern.compile(loginNameRegex);
		Matcher loginNameMatcher = loginNamePattern.matcher(loginName);
		boolean loginNameMatches = loginNameMatcher.matches();
		if (loginNameMatches) {
			return true;
		}
		return false;
	}
}
